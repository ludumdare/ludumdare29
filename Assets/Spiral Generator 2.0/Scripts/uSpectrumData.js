// 	Copyright 2013 Unluck Software
//	www.chemicalbliss.com
//
//	This script is just a demonstration on how the spiral generator can be used with spectrum data
// Ignore the GetSpectrumData warning
var growSpeed : float = 10.0;
var useFrequency : boolean = true;
var growFrequency : float = 5.0;  
var growFrequencySpeed : float = 5.0;
var channel : int = 1;
var numSamples : int = 256;
var freq : int = 2;
var number = new Array ();

private var _audio:AudioListener;
private var _spiral:SpiralGenerator;
private var multiBuffer:float;
private var heightBuffer:float;
private var depthBuffer:float;

function Start () {
	_spiral = transform.GetComponent(SpiralGenerator);
	multiBuffer = _spiral.sineGrowMultiplier;
	heightBuffer = _spiral.sineHeightFrequency;
	depthBuffer = _spiral.sineDepthFrequency;
	_audio = Camera.mainCamera.GetComponent(AudioListener);
}
   
    
function Update () {    
	var number = _audio.GetSpectrumData(numSamples, channel, FFTWindow.Rectangular);
//	var number = _audio.GetOutputData(numSamples, channel);
	var dif = multiBuffer*number[freq]*10 - _spiral.sineGrowMultiplier;
	_spiral.sineGrowMultiplier += dif*Time.deltaTime*growSpeed;
	if(useFrequency){
		dif = heightBuffer*number[freq]*growFrequency - _spiral.sineHeightFrequency;
		_spiral.sineHeightFrequency += dif*Time.deltaTime*growFrequencySpeed;
		dif = depthBuffer*number[freq]*growFrequency - _spiral.sineDepthFrequency;
		_spiral.sineDepthFrequency += dif*Time.deltaTime*growFrequencySpeed;
	}
}