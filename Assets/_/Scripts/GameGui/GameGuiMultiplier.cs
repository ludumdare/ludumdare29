﻿using UnityEngine;
using System.Collections;

public class GameGuiMultiplier : MonoBehaviour {

	private tk2dTextMesh		m_textMesh;
	
	//	PUBLIC
	
	//	PRIVATE
	
	//	MONO
	
	
	void OnEnable() {
		
		D.Trace(LogCategory.SubSystem, "[GameGuiMultiplier] OnEnable");
		m_textMesh = transform.GetComponent<tk2dTextMesh>();
	}
	
	void Start () {
		
		D.Trace(LogCategory.SubSystem, "[GameGuiMultiplier] Start");
	}
	
	// Update is called once per frame
	void Update () {
		
		D.Fine(LogCategory.SubSystem, "[GameGuiMultiplier] Update");
		m_textMesh.text = string.Format("{0}x", GameController.Instance.GameScore.Multiplier);
	}
}
