﻿using UnityEngine;
using System.Collections;

public class GameGuiLives : MonoBehaviour {

	public tk2dSprite[]			m_heartSprites;
	private tk2dTextMesh		m_textMesh;

	
	//	PUBLIC
	
	//	PRIVATE

	private IEnumerator __updateLives() {

		yield return new WaitForSeconds(2);
		int lives = GameController.Instance.GameLives.Lives - 1;

		for(int i = 0; i < m_heartSprites.Length; i++) {

			if(i <= lives) {
				m_heartSprites[i].renderer.enabled = true;
			} else {
				m_heartSprites[i].renderer.enabled = false;
			}
		}

		yield return StartCoroutine(__updateLives());
	}
	
	//	MONO
	
	
	void OnEnable() {
		
		D.Trace(LogCategory.SubSystem, "[GameGuiLives] OnEnable");
		m_textMesh = transform.GetComponent<tk2dTextMesh>();
		m_textMesh.renderer.enabled = false;
	}
	
	void Start () {
		
		D.Trace(LogCategory.SubSystem, "[GameGuiLives] Start");
		StartCoroutine(__updateLives());
	}
	
	// Update is called once per frame
	void Update () {
		
		D.Fine(LogCategory.SubSystem, "[GameGuiLives] Update");
		//m_textMesh.text = string.Format("Shape Force {0}", GameController.Instance.GameLives.Lives);
	}
}
