﻿
//	Copyright 2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class GameGuiScore : MonoBehaviour {

	private tk2dTextMesh		m_textMesh;

	//	PUBLIC

	//	PRIVATE

	//	MONO


	void OnEnable() {

		D.Trace(LogCategory.SubSystem, "[GameGuiScore] OnEnable");
		m_textMesh = transform.GetComponent<tk2dTextMesh>();
	}

	void Start () {
	
		D.Trace(LogCategory.SubSystem, "[GameGuiScore] Start");
	}
	
	// Update is called once per frame
	void Update () {

		D.Fine(LogCategory.SubSystem, "[GameGuiScore] Update");
		m_textMesh.text = string.Format("Score {0}", GameController.Instance.GameScore.Score);
	}
}
