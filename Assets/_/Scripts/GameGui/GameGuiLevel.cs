﻿using UnityEngine;
using System.Collections;

public class GameGuiLevel : MonoBehaviour {

	public tk2dSlicedSprite		m_LevelSprite;

	private tk2dTextMesh		m_textMesh;

	
	//	PUBLIC
	
	//	PRIVATE

	private IEnumerator __updateLevel() {

		yield return new WaitForSeconds(1);
		float size =  (float)GameController.Instance.GameLevel.Points/(float)GameController.Instance.GameLevel.NextLevel;
		float s = 250*size;
		D.Log(s);
		m_LevelSprite.dimensions = new Vector2(s,8);
		yield return StartCoroutine(__updateLevel());
	}

	//	MONO
	
	
	void OnEnable() {
		
		D.Trace(LogCategory.SubSystem, "[GameGuiLevel] OnEnable");
		m_textMesh = transform.GetComponent<tk2dTextMesh>();
	}
	
	void Start () {
		
		D.Trace(LogCategory.SubSystem, "[GameGuiLevel] Start");
		StartCoroutine(__updateLevel());
	}
	
	// Update is called once per frame
	void Update () {
		
		D.Fine(LogCategory.SubSystem, "[GameGuiLevel] Update");
		m_textMesh.text = string.Format("{0}", GameController.Instance.GameLevel.Level);
	}
}
