﻿
//	Copyright 2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class GameGuiTimer : MonoBehaviour {

	private tk2dTextMesh		m_textMesh;
	private GameTimer			m_timer;

	//	PUBLIC

	//	PRIVATE

	//	MONO

	void OnEnable() {

		D.Trace("[GameGuiTimer] OnEnable");
		m_textMesh = transform.GetComponent<tk2dTextMesh>();
	}

	void Start () {

		D.Trace("[GameGuiTimer] Start");
		m_timer = GameController.Instance.GameTimer;
	}
	
	void Update () {

		if (m_timer.GetFormattedTime() != null) m_textMesh.text = m_timer.GetFormattedTime();
	
	}
}
