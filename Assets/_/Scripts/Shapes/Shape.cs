﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Holoville.HOTween;

public class Shape : MonoBehaviour {

	public GameShape	ShapeGameShape	{ get; set; }	//
	public string		ShapeId			{ get; set; }	//	shape sprite id
	public Color		ShapeColor		{ get; set; }	//	shape color
	public int			ShapePattern	{ get; set; }	//	shape pattern
	public bool			ShapeDragging	{ get; set; }	//	shape is dragging
	public Vector2		ShapeDirection	{ get; set; }	//	dir of shape travel
	public float		ShapeSpeed		{ get; set; }	//	shape lane speed
	public Vector2		ShapeStartPos	{ get; set; }	//	shape starting position

	public Vector2		ShapeLaneBegin	{ get; set; }	//	shape lane start position
	public Vector2		ShapeLaneEnd	{ get; set; }	//	shape lane end position

	private string		m_shape;
	private Color		m_shapeColor;
	private string		m_shapePattern;

	private int			m_eyeCount;

	//private tk2dSprite	m_spriteShape;
	private tk2dSprite	m_spriteEyes;

	private Tweener		m_tweener;

	private bool		m_inShifter;
	private bool		m_isMoving;

	//	PUBLIC

	public void GenerateShape() {

		D.Trace(LogCategory.Game, "[Shape] GenerateShape");
		__generateShape();
	}


	public void StartMoving(Vector2 dir) {
		
		D.Trace(LogCategory.Game, "[Shape] StartMoving");
		__startMoving(dir);
	}

	public void Pause() {

		m_tweener.Pause();
	}

	public void Play() {

		if (m_inShifter) {
			GameController.Instance.Shifter.ShapeShift(this);
			return;
		}

		m_tweener.Play();
	}

	public void Kill() {

		TweenParms tp = new TweenParms();
		tp.Prop("position", new Vector3(Random.Range(-600,600), Random.Range(-400,400), 0));
		tp.OnComplete(__onKillComplete);

		TweenParms tp1 = new TweenParms();
		tp1.Prop("eulerAngles", new Vector3(0,0,359f));
		tp1.Loops(-1,LoopType.Yoyo);

		HOTween.To(transform, 1, tp);
		HOTween.To(transform, 1, tp1);
	}

	private void __onKillComplete(TweenEvent e) {
		GameManager.Instance.GameEffects.PlaySmallExplosion(transform.position);
		Destroy(gameObject);
	}

	public void StartPickup() {

		TweenParms tp = new TweenParms();
		transform.eulerAngles = new Vector3(0,0,340);
		tp.Prop("eulerAngles", new Vector3(0,0,380), false);
		tp.Loops(-1, LoopType.Yoyo);
		m_tweener = HOTween.To(transform, 0.5f, tp);
	}

	public void StopPickup() {

		m_tweener.Kill();
		HOTween.To(transform, 1, "eulerAngles", new Vector3(0,0,0));
	}
	
	public void TriggerEnter(Collider2D other) {
		
		D.Trace(LogCategory.Game, "[Shape] OnTriggerEnter");
	}
	
	public void TriggerExit(Collider2D other) {
		
		D.Trace(LogCategory.Game, "[Shape] OnTriggerExit");
	}
	
	//	PRIVATE

	private void __generateShape() {

		D.Trace(LogCategory.Game, "[Shape] __generateShape");

		//	make sure we dont have a shape attached already
		if (transform.FindChild("#Shape") != null) return;

		//	get random shape from the shape manager
		GameShape shape = GameController.Instance.GameShapeManager.GetRandomShape();

		//	make it a child
		shape.gameObject.name = "#Shape";
		shape.gameObject.SetActive(true);
		shape.transform.parent = this.transform;
		shape.transform.localPosition = new Vector3(0,0,0);
		this.transform.parent = GameObject.Find("_Shapes").transform;

		//	rescale it
		shape.transform.localScale = new Vector3(0.20f, 0.20f, 1f);
		shape.GetComponent<tk2dSprite>().SortingOrder = -2;

		//	color it
		ShapeColor = GameController.Instance.GameSwatch.GetRandomShapeColor();
		shape.GetComponent<tk2dSprite>().color = ShapeColor;

		//	housekeeping
		ShapeGameShape = shape;
		ShapeId = shape.ShapeId;

		//m_spriteShape.SetSprite(Random.Range(0, m_shapeCount));
		//transform.GetComponent<Collider2D>().isTrigger = true;
	}

	private void __startMoving(Vector2 dir) {

		D.Trace(LogCategory.Game, "[Shape] __startMoving");

		ShapeDirection = dir * ShapeSpeed;
		transform.position = ShapeStartPos;
		m_isMoving = true;

		/*
		transform.position = (Vector3)ShapeLaneBegin;

		TweenParms tp = new TweenParms();
		tp.Prop("position", (Vector3)ShapeLaneEnd);
		tp.SpeedBased();
		tp.OnComplete(__tweenOnComplete);
		m_tweener = HOTween.To(transform, ShapeSpeed, tp);
		*/
	}

	private void __tweenOnComplete(TweenEvent e) {

		D.Trace(LogCategory.Game, "[Shape] __startMoving");
		Destroy(gameObject);
	}

	//	MONO

	void OnEnable() {

		D.Trace(LogCategory.Game, "[Shape] OnEnable");
		//m_spriteShape = transform.GetComponent<tk2dSprite>();
		m_spriteEyes = transform.FindChild("#Eyes").GetComponent<tk2dSprite>();
		m_eyeCount = m_spriteEyes.Collection.Count;
	}

	void Start () {

		D.Trace(LogCategory.Game, "[Shape] Start");
	}


	void Update () {
	
		D.Fine(LogCategory.Game, "[Shape] Update");

		if (!m_isMoving) return;
		if (ShapeDragging) return;

		if (transform.position.y < -1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}

		if (transform.position.y > 1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		if (transform.position.x < -700) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		if (transform.position.x > 700) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		transform.Translate(ShapeDirection * Time.deltaTime);

	}

}
