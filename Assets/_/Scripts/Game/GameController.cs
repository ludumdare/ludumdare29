
//	Copyright 2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameController : MonoSingleton<GameController> {
	
	public GameCursor			GameCursor			{ get; set; }
	public GameInput			GameInput			{ get; set; }
	public GameLevel			GameLevel			{ get; set; }
	public GameLives			GameLives			{ get; set; }
	public GameTimer			GameTimer			{ get; set; }
	public GameScore			GameScore			{ get; set; }
	public GameShapeManager		GameShapeManager	{ get; set; }
	public GameSpawner			GameSpawner			{ get; set; }
	public GameStars			GameStars			{ get; set; }
	public GameSwatch			GameSwatch			{ get; set; }
	public Shifter				Shifter				{ get; set; }

	public bool					GodMode				{ get; set; }

	private GameManager			m_manager;

	private GameOverPanel		m_gameOverPanel;

	private GameObject			m_stars;
	private GameObject			m_starsWarp;

	//	PUBLIC
	
	public void Play() {

		D.Log("[GameController] Play");
		__play();
	}

	public void GameOver() {

		D.Log("[GameController] GameOver");
		m_gameOverPanel.Show();
	}

	//	PRIVATE

	private void __play() {

		D.Log("[GameController] __play");

		GameManager.Instance.GameMusic.PlayGameMusic();

		GameSpawner.SpawnRate = 30;
		GameSpawner.SpawnTime = 60;
		GameSpawner.SpawnSpeed = 20;
		GameSpawner.SpawnAreas = 0;

		m_stars.SetActive(true);
		m_starsWarp.SetActive(false);

		GameTimer.StartTimer();
		GameSpawner.StartSpawner();
		GameManager.Instance.GameMusic.SetPitch(1);
		GameLives.Lives = 3;
	}

	private void __gameTimerCompletedEvent(GameTimerCompletedEvent e) {

		GameSpawner.StopSpawner();
		HOTween.Pause();
		m_gameOverPanel.Show();

	}

	private void __gameOverEvent(GameOverEvent e) {

		m_gameOverPanel = GameObject.Find("GameOverPanel").GetComponent<GameOverPanel>();

		GameSpawner.StopSpawner();
		GameTimer.StopTimer();
		HOTween.Pause();
		HOTween.To(Shifter.transform, 5, "position", new Vector3(Shifter.transform.position.x, 1000,0));
		__removeShapes();
		GameObject.Find("GameSubmitPanel").GetComponent<SDNPanel>().Show();
		GameObject.Find("PauseButton").GetComponent<SDNPanel>().Hide();

	}

	private void __removeShapes() {

		GameObject[] shapes = GameObject.FindGameObjectsWithTag("Shape");
		foreach(GameObject obj in shapes) {
			Destroy(obj);
		}
	}

	private IEnumerator __advanceLevel() {

		//	show sector completed panel
		yield return new WaitForSeconds(1);
		GameObject.Find("@GameSectorCompleted").GetComponent<tk2dTextMesh>().text = string.Format("SECTOR {0}\nCOMPLETE", GameLevel.Level - 1);
		GameObject.Find("GameLevelPanel").GetComponent<SDNPanel>().Show();

		//	move shifter out of view
		yield return new WaitForSeconds(1);
		HOTween.To(Shifter.transform, 1, "position", new Vector3(Shifter.transform.position.x, 800, 0));

		//	remove level completed panel
		yield return new WaitForSeconds(2);
		m_stars.SetActive(true);
		m_starsWarp.SetActive(false);
		GameObject.Find("GameLevelPanel").GetComponent<SDNPanel>().Hide();

		//	show get ready
		//yield return new WaitForSeconds(1);
		//GameObject.Find("GameReadyPanel").GetComponent<SDNPanel>().Show();
		
		//	bring shifter into view
		yield return new WaitForSeconds(2);
		Shifter.transform.position = new Vector2(Shifter.transform.position.x, -800);
		GameObject.Find("GameReadyPanel").GetComponent<SDNPanel>().Hide();
		HOTween.To(Shifter.transform, 1, "position", new Vector3(Shifter.transform.position.x, -150, 0));

		//	start next level
		yield return new WaitForSeconds(1);

		GameTimer.StartTimer();
		GameSpawner.StartSpawner();

		//	show go!
		//yield return new WaitForSeconds(1);
		//GameObject.Find("GameGoPanel").GetComponent<SDNPanel>().Show();
		
		//	hide go!
		//yield return new WaitForSeconds(2);
		//GameObject.Find("GameGoPanel").GetComponent<SDNPanel>().Hide();
		
		yield return null;
	}

	private void __advanceLevelEvent(GameAdvanceLevelEvent e) {
	
		m_stars.SetActive(false);
		m_starsWarp.SetActive(true);
		GameSpawner.StopSpawner();
		GameTimer.StopTimer();
		GameManager.Instance.GameMusic.SetPitch(1.0f + (GameLevel.Level * .001f));

		__removeShapes();

		//	increase speed every level
		GameSpawner.SpawnSpeed += 10;

		//	increase level of objects per second
		GameSpawner.SpawnRate += 10;


		if (GameLevel.Level >= 5) {

			GameSpawner.SpawnSpeed = 60;
			GameSpawner.SpawnRate = 45;
			GameSpawner.SpawnAreas = 1;
		}

		if (GameLevel.Level >= 10) {

			GameSpawner.SpawnSpeed = 85;
			GameSpawner.SpawnRate = 50;
			GameSpawner.SpawnAreas = 2;
		}

		if (GameLevel.Level >= 15) {

			GameSpawner.SpawnSpeed = 85;
			GameSpawner.SpawnRate = 55;
			GameSpawner.SpawnAreas = 3;
		}

		if (GameLevel.Level >= 20) {
			
			GameSpawner.SpawnSpeed = 110;
			GameSpawner.SpawnRate = 80;
			GameSpawner.SpawnAreas = 4;
		}
		
		if (GameLevel.Level >= 25) {
			
			GameSpawner.SpawnSpeed = 135;
			GameSpawner.SpawnRate = 85;
			GameSpawner.SpawnAreas = 5;
		}
		
		if (GameLevel.Level >= 30) {
			
			GameSpawner.SpawnSpeed = 160;
			GameSpawner.SpawnRate = 90;
			GameSpawner.SpawnAreas = 5;
		}
		
		StartCoroutine(__advanceLevel());
		StartCoroutine(__fireworks());
	}

	private IEnumerator __fireworks() {

		for(int i = 0; i < 10; i++) {

			GameManager.Instance.GameEffects.PlayFireworkd(new Vector2(Random.Range(-200,200), Random.Range(-200,200)));
			yield return new WaitForSeconds(.5f);
		}
	}


	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[GameController] OnEnable");
		m_manager = GameManager.Instance;
		m_manager.GameController = this;

		GameEvents.Instance.AddListener<GameTimerCompletedEvent>(__gameTimerCompletedEvent);
		GameEvents.Instance.AddListener<GameOverEvent>(__gameOverEvent);
		GameEvents.Instance.AddListener<GameAdvanceLevelEvent>(__advanceLevelEvent);

		m_stars = GameObject.Find("GameStars");
		m_starsWarp = GameObject.Find("GameStarsWarp");
	}
	
	void Start() {
	
		D.Trace("[GameController] Start");
	}

	void OnDisable() {

		GameEvents.Instance.RemoveListener<GameTimerCompletedEvent>(__gameTimerCompletedEvent);
		GameEvents.Instance.RemoveListener<GameOverEvent>(__gameOverEvent);
		GameEvents.Instance.RemoveListener<GameAdvanceLevelEvent>(__advanceLevelEvent);
	}
}
