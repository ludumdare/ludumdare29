﻿using UnityEngine;
using System.Collections;

public class GameSwatch : MonoBehaviour {

	public Color[]			ShapeColors;
	public Color[]			ButtonColors;
	public Color[]			ScreenShakeColors;

	//	PUBLIC

	public Color GetRandomShapeColor() {

		D.Trace(LogCategory.System, "[GameSwatch] GetRandomShapeColor");
		return __getRandomShapeColor();
	}

	public Color GetRandomScreenShakeColor() {
		
		D.Trace(LogCategory.System, "[GameSwatch] GetRandomScreenShakeColor");
		return __getRandomScreenShakeColor();
	}
	
	//	PRIVATE

	private Color __getRandomShapeColor() {

		D.Trace(LogCategory.System, "[GameSwatch] __getRandomShapeColor");
		return ShapeColors[Random.Range(0, ShapeColors.Length)];
	}

	private Color __getRandomScreenShakeColor() {
		
		D.Trace(LogCategory.System, "[GameSwatch] __getRandomScreenShakeColor");
		return ScreenShakeColors[Random.Range(0, ScreenShakeColors.Length)];
	}
	
	//	MONO

	void OnEnable() {

		D.Trace(LogCategory.System, "[GameSwatch] OnEnable");
		GameController.Instance.GameSwatch = this;
	}

	void Start () {
	
		D.Trace(LogCategory.System, "[GameSwatch] Start");
	}
	
}
