﻿using UnityEngine;
using System.Collections;

public class GameLevel : MonoBehaviour {

	public int			Level		{ get; set; }
	public int			Points		{ get; set; }
	public int			NextLevel	{ get; set; }

	private int			m_points;

	//	PUBLIC

	public void AddPoints(int points) {

		m_points += points;

		if (m_points >= NextLevel) __changeLevel(Level + 1);

		Points = m_points;
	}

	public void ChangeLevel(int level) {
		__changeLevel(level);
	}

	//	PRIVATE

	private void __changeLevel(int level) {

		Level = level;
		NextLevel = Level * 5;
		m_points = 0;

		GameManager.Instance.GameSounds.PlayLevelUp();
		GameEvents.Instance.Raise(new GameAdvanceLevelEvent());
	}

	//	MONO

	void OnEnable() {

		GameController.Instance.GameLevel = this;
		Level = 1;
		NextLevel = 10;
		m_points = 0;
	}

	void Start () {
	
	}
	
	void Update () {
	
	}
}
