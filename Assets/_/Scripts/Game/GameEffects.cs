﻿using UnityEngine;
using System.Collections;

public class GameEffects : MonoSingleton<GameEffects> {

	public GameObject		EffectPickupHeart;
	public GameObject		EffectPickupBonus;
	public GameObject		EffectSmallExplosion;
	public GameObject		EffectSmallHit;
	public GameObject		EffectAura;
	public GameObject		EffectFireworks;

	public void PlayPickupHeart(Vector2 pos) {
		__playEffect(EffectPickupHeart, pos);
	}

	public void PlayBonus(Vector2 pos) {
		__playEffect(EffectPickupBonus, pos);
	}

	public void PlaySmallExplosion(Vector2 pos) {
		__playEffect(EffectSmallExplosion, pos);
	}

	public void PlaySmallHit(Vector2 pos) {
		__playEffect(EffectSmallHit, pos);
	}
	
	public GameObject PlayAura(Vector2 pos) {
		return __playEffect(EffectAura, pos);
	}
	
	public GameObject PlayFireworkd(Vector2 pos) {
		return __playEffect(EffectFireworks, pos);
	}
	
	private GameObject __playEffect(GameObject obj, Vector2 pos) {

		GameObject particles = (GameObject)Instantiate(obj);
		particles.transform.position = pos;
		#if UNITY_3_5
		particles.SetActiveRecursively(true);
		#else
		particles.SetActive(true);
		for(int i = 0; i < particles.transform.childCount; i++)
			particles.transform.GetChild(i).gameObject.SetActive(true);
		#endif
		
		ParticleSystem ps = particles.GetComponent<ParticleSystem>();
		if(ps != null && ps.loop)
		{
			ps.gameObject.AddComponent<CFX3_AutoStopLoopedEffect>();
			ps.gameObject.AddComponent<CFX_AutoDestructShuriken>();
		}

		return particles;

	}

	void OnEnable() {
		GameManager.Instance.GameEffects = this;
	}
	
}
