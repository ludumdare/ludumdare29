﻿using UnityEngine;
using System.Collections;

public class GameLives : MonoBehaviour {

	public int 		Lives	{ get; set; }

	//	PUBLIC

	public void AddLives(int lives) {
		Lives += lives;
		if (Lives > 6) Lives = 6;
	}

	public void RemoveLives(int lives) {
		Lives -= lives;
		if (Lives <= 0) GameEvents.Instance.Raise(new GameOverEvent());
	}

	//	PRIVATE

	//	MONO

	void OnEnable() {

		GameController.Instance.GameLives = this;
	}

}
