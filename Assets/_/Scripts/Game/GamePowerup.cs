﻿using UnityEngine;
using System.Collections;

public class GamePowerup : MonoBehaviour {

	public string		PowerId;
	public string 		PowerName;

	private bool m_isMoving;
	private Vector2 m_dir;
	
	public void StartMoving(Vector2 dir, Vector2 pos) {
		
		D.Trace(LogCategory.Game, "[GamePowerup] StartMoving");
		__startMoving(dir, pos);
	}
	
	private void __startMoving(Vector2 dir, Vector2 pos) {
		
		D.Trace(LogCategory.Game, "[GamePowerup] __startMoving");
		
		m_dir = dir;
		transform.position = pos;
		m_isMoving = true;
		
		/*
		transform.position = (Vector3)ShapeLaneBegin;

		TweenParms tp = new TweenParms();
		tp.Prop("position", (Vector3)ShapeLaneEnd);
		tp.SpeedBased();
		tp.OnComplete(__tweenOnComplete);
		m_tweener = HOTween.To(transform, ShapeSpeed, tp);
		*/
	}
	void Update () {
		
		D.Fine(LogCategory.Game, "[GamePowerup] Update");
		
		if (!m_isMoving) return;
		
		if (transform.position.y < -1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		if (transform.position.y > 1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		if (transform.position.x < -1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		if (transform.position.x > 1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		
		transform.Translate(m_dir * Time.deltaTime);
		
	}

}
