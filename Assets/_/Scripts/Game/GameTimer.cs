
//	Copyright 2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class GameTimer : MonoBehaviour {
	
	public int					m_duration = 30;
	public bool					m_timerDown;
	
	private bool				m_started;
	private float				m_startTime;
	
	private float				m_time;
	private string				m_formattedTime;

	//	PUBLIC

	public void AddTime(int time) {

		D.Trace(LogCategory.SubSystem, "[GameTimer] AddTime");
		__startTimer((int)( m_time + time));
	}

	public void RemoveTime(int time) {

		D.Trace(LogCategory.SubSystem, "[GameTimer] RemoveTime");
		__startTimer((int)(m_time - time));
	}

	public float GetTime() {
		
		D.Fine(LogCategory.SubSystem, "[GameTimer] GetTime");
		return m_time;	
	}

	public string GetFormattedTime() {

		D.Fine(LogCategory.SubSystem, "[GameTimer] GetFormattedTime");
		return m_formattedTime;
	}
	
	public void StartTimer() {
		
		D.Trace(LogCategory.SubSystem, "[GameTimer] StartTimer");
		__startTimer(m_duration);
	}	
	
	public void StartTimer(int duration) {
		
		D.Trace(LogCategory.SubSystem, "[GameTimer] StartTimer(duration)");
		__startTimer(duration);
	}	
	
	public void StopTimer() {
		
		D.Trace(LogCategory.SubSystem, "[GameTimer] StopTimer");
		m_started = false;
		D.Detail(LogCategory.GUIEvent, "- raising GameTimerStopEvent");
		GameEvents.Instance.Raise(new GameTimerStopEvent());
	}
	
	public void SetTimer(int seconds) {
		
		D.Trace("[GameTimer] TimerSet");
		m_started = false;
		m_duration = seconds;
	}
	
	//	PRIVATE

	private void __startTimer(int duration) {

		D.Trace(LogCategory.SubSystem, "[GameTimer] __timerStart(duration)");
		m_duration = duration;
		m_started = true;
		m_startTime = Time.time;
		if (!m_timerDown) m_time = 0.0f;
		D.Detail(LogCategory.GUIEvent, "- raising GameTimerStartEvent");
		GameEvents.Instance.Raise(new GameTimerStartEvent());
	}

	//	MONO

	void OnEnable() {
		GameController.Instance.GameTimer = this;	
	}
	
	void Update () {
		
		D.Fine("[GameTimer] Update");
		if (m_started) {
			__timer();
		}
	}
	
	private void __timer() {
		
		D.Fine("[GameTimer] __timer");
		
		int _rseconds;
		
		if (m_timerDown) {
			
			float _timeLeft = Time.time - m_startTime;
			float _rsec = m_duration - _timeLeft;
			_rseconds = Mathf.CeilToInt(_rsec);
			
			if (_rseconds < 0 ) {
				_rseconds = 0;
				m_started = false;
				D.Detail(LogCategory.GUIEvent, "- raising GameTimerCompletedEvent");
				GameEvents.Instance.Raise(new GameTimerCompletedEvent());
			}
			
		} else {
			
			m_time += Time.deltaTime;
			_rseconds = Mathf.CeilToInt(m_time);
		}
		
		int _sec = _rseconds % 60;
		int _min = _rseconds / 60;
		
		string _formattedTime = System.String.Format("{0:0}:{1:00}", _min, _sec);

		m_formattedTime = _formattedTime;

		if (m_timerDown) m_time = _rseconds;
		
		D.Watch("GameTimer", "time", m_formattedTime);

	}
	
}
