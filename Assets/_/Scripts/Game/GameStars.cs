﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameStars : MonoBehaviour {

	public ParticleSystem		LargeStars;
	public ParticleSystem		SmallStars;
	public int					Delay = 5;

	private IEnumerator __shiftStars() {

		D.Log("[GameStars] __shiftStars");

		yield return new WaitForSeconds(Delay);

		int x,y,z;

		x = Random.Range(230,300);
		y = -90;
		z = 90;

		D.Log("- shifting stars");

		HOTween.To(LargeStars.transform, 3, "eulerAngles", new Vector3(x,y,z));
		HOTween.To(SmallStars.transform, 2, "eulerAngles", new Vector3(x,y,z));

		yield return StartCoroutine(__shiftStars());
	}

	void OnEnable() {
	}

	// Use this for initialization
	void Start () {
	
		StartCoroutine(__shiftStars());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
