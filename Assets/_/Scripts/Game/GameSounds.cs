﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSounds : MonoBehaviour {
	
	public List<AudioClip>		m_random;
	public List<AudioClip>		m_mysterious;
	
	public AudioClip			m_startLevel;
	public AudioClip			m_consumeShape;
	public AudioClip			m_badShape;
	public AudioClip			m_levelUp;
	public AudioClip			m_multiplierUp;
	public AudioClip			m_multiplierReset;
	public AudioClip			m_bonusPickup;
	public AudioClip			m_powerPickup;
	

	public void PlayRandom() {
	
		__playSoundOnce(__getRandomClip(m_random));	
	}
	
	public void PlayMysterious() {
		__playSoundOnce(__getRandomClip(m_mysterious));	
	}
	
	public void PlayStartLevel() {
		__playSoundOnce(m_startLevel);		
	}

	public void PlayConsumeShape() {
		__playSoundOnce(m_consumeShape);		
	}
	
	public void PlayBadShape() {
		__playSoundOnce(m_badShape);		
	}
	
	public void PlayLevelUp() {
		__playSoundOnce(m_levelUp);		
	}
	
	public void PlayMultiplierUp() {
		__playSoundOnce(m_multiplierUp);		
	}
	
	public void PlayMultiplierReset() {
		__playSoundOnce(m_multiplierReset);		
	}

	public void PlayBonusPickup() {
		__playSoundOnce(m_bonusPickup);		
	}

	public void PlayPowerPickup() {
		__playSoundOnce(m_powerPickup);		
	}
	
	private AudioClip __getRandomClip(List<AudioClip> list) {
		D.Trace("[GameSounds] __getRandomClip");
		int i = Random.Range(0, list.Count-1);
		D.Log(i);
		return list[i];
	}
	
	private void __playSoundOnce(AudioClip clip) {
		D.Trace("[GameSounds] __playSoundOnce");
		audio.PlayOneShot(clip);
	}
	
	void OnEnable() {
		D.Trace("[GameSounds] OnEnable");
		GameManager.Instance.GameSounds = this;
	}
}
