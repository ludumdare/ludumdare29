﻿using UnityEngine;
using System.Collections;

public class GameInput : MonoBehaviour {

	//	PUBLIC

	public RaycastHit2D GetRaycastHit() {

		D.Fine(LogCategory.SubSystem, "[GameInput] GetRaycastHit");
		return __getRaycastHit();
	}

	public Vector2 GetMousePosition() {

		D.Fine(LogCategory.SubSystem, "[GameInput] GetMousePosition");
		return __getMousePosition();
	}

	//	PRIVATE

	private RaycastHit2D __getRaycastHit() {
		
		D.Fine(LogCategory.SubSystem, "[GameInput] __getRaycastHit");

		//	raycast from mouse/touch to screen position
		
		Vector2 pos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
		RaycastHit2D hitInfo = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(pos), Vector2.zero);
		
		// RaycastHit2D can be either true or null, but has an implicit conversion to bool, so we can use it like this
		
		return hitInfo;
	}

	private Vector2 __getMousePosition() {

		D.Fine(LogCategory.SubSystem, "[GameInput] __getMousePosition");
		return (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}
	
	//	MONO

	void OnEnable() {

		D.Trace(LogCategory.SubSystem, "[GameInput] OnEnable");
	}

	void Start () {
	
		D.Trace(LogCategory.SubSystem, "[GameInput] Start");
		GameController.Instance.GameInput = this;
	}
	
}
