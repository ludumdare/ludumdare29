﻿
//	Copyright 2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class GameSpawner : MonoBehaviour {
		
	public Shape			SpawnShape;					//	the shape prefab
	public GameBonus[]		SpawnBonus;					//	bonus items	
	public GamePowerup[]	SpawnPowerup;				//	powerups

	public int			SpawnRate	{ get; set; }	//	number of shapes to generate over a time period
	public int			SpawnTime	{ get; set; }	//	time period in which rate of shapes will generate
	public int			SpawnSpeed	{ get; set; }	//	variable adjustment for speed
	public int			SpawnAreas	{ get; set; }	//	which spawn areas are open


	private float		m_spawnDelay;		//	time between shapes spawning on the scene

	private int			m_totalBonus;
	private int			m_totalPower;

	private GameController	m_controller;

	//	PUBLIC

	public void StartSpawner() {

		D.Trace(LogCategory.Game, "[GameSpawner] StartSpawner");
		__start();
	}

	public void StopSpawner() {

		D.Trace(LogCategory.Game, "[GameSpawner] StopSpawner");
		__stop();

	}

	//	PRIVATE

	private void __start() {

		D.Trace(LogCategory.Game, "[GameSpawner] __start");

		//	calculate the delay between shapes spawning
		m_spawnDelay = (float)((float)SpawnTime / (float)SpawnRate);
		
		D.Log("- spawner delay is set to {0:F2} seconds", m_spawnDelay);
		StartCoroutine(__spawner());
	}

	private void __stop() {

		StopAllCoroutines();
	}

	private IEnumerator __spawner() {

		D.Trace(LogCategory.Game, "[GameSpawner] __spawner");
		D.Detail("- waiting to spawn in {0:F2} seconds", m_spawnDelay);
		yield return new WaitForSeconds(m_spawnDelay);
		__spawn();
		yield return StartCoroutine(__spawner());

	}

	private void __spawn() {

		int i = Random.Range(0,200);

		if (i < 170) {
			__spawnShape();
			return;
		}

		if (i < 190) {
			__spawnBonus();
			return;
		}

		__spawnPowerup();
	}

	private void __spawnBonus() {

		D.Trace(LogCategory.Game, "[GameSpawner] __spawnBonus");
		D.Detail("- spawning new bonus");
		GameBonus bonus = SpawnBonus[Random.Range(0,m_totalBonus)];
		GameBonus _s = (GameBonus)Instantiate(bonus);
		_s.gameObject.SetActive(true);
		float _speed = 60f + (60f/m_spawnDelay) + Random.Range(10f + SpawnSpeed, 50f + SpawnSpeed);

		Vector2 dir = Vector2.zero;
		Vector2 pos = Vector2.zero;

		dir = -Vector2.up * _speed;

		int i = Random.Range(0,3);

		if (i==0) {

			//	somewhere near the top
			pos = new Vector2(Random.Range(-600,600), 400);
			dir = -Vector2.up * _speed;
		}

		if (i==1) {

			//	somewhere near the right
			pos = new Vector2(800, Random.Range(-300,300));
			dir = -Vector2.right * _speed;
		}

		if (i==2) {

			//	somewhere on the left
			pos = new Vector2(-800, Random.Range(-300,300));
			dir = Vector2.right * _speed;
		}

		_s.StartMoving(dir, pos);
	}

	private void __spawnPowerup() {

		D.Trace(LogCategory.Game, "[GameSpawner] __spawnPowerup");

		D.Detail("- spawning new powerup");
		GamePowerup power = SpawnPowerup[Random.Range(0,m_totalPower)];
		GamePowerup _s = (GamePowerup)Instantiate(power);
		_s.gameObject.SetActive(true);
		float _speed = 60f + (60f/m_spawnDelay) + Random.Range(10f + SpawnSpeed, 50f + SpawnSpeed);
		
		Vector2 dir = Vector2.zero;
		Vector2 pos = Vector2.zero;
		
		dir = -Vector2.up * _speed;
		
		int i = Random.Range(0,3);
		
		if (i==0) {
			
			//	somewhere near the top
			pos = new Vector2(Random.Range(-600,600), 400);
			dir = -Vector2.up * _speed;
		}
		
		if (i==1) {
			
			//	somewhere near the right
			pos = new Vector2(800, Random.Range(-300,300));
			dir = -Vector2.right * _speed;
		}
		
		if (i==2) {
			
			//	somewhere on the left
			pos = new Vector2(-800, Random.Range(-300,300));
			dir = Vector2.right * _speed;
		}
		
		_s.StartMoving(dir, pos);
	}

	private void __spawnShape() {

		D.Trace(LogCategory.Game, "[GameSpawner] SpawnerStart");
		D.Detail("- spawning new shape");
		Shape _s = (Shape)Instantiate(SpawnShape);
		_s.gameObject.SetActive(true);
		_s.GenerateShape();
		_s.ShapeSpeed = 60f + (60f/m_spawnDelay) + Random.Range(10f + SpawnSpeed, 50f + SpawnSpeed);

		//	somewhere near the top
		_s.ShapeStartPos = new Vector2(Random.Range(-600,600), 400);
		_s.StartMoving(-Vector2.up);
		D.Detail("- new shape has spawned as location {1} at a speed of {0:F2}", _s.ShapeSpeed, _s.ShapeStartPos);

	}



	//	MONO

	void OnEnable() {

		D.Trace(LogCategory.Game, "[GameSpawner] OnEnable");
		m_controller = GameController.Instance;
		m_totalBonus = SpawnBonus.Length;
		m_totalPower = SpawnPowerup.Length;
		m_controller.GameSpawner = this;

	}

	void Start () {

		D.Trace(LogCategory.Game, "[GameSpawner] Start");

	}
	
	void Update () {
	
		D.Fine(LogCategory.Game, "[GameSpawner] OnEnable");
	}
}
