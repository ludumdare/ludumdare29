﻿using UnityEngine;
using System.Collections;

public class GameBonus : MonoBehaviour {

	public string 		BonusId;
	public string		BonusName;
	public int			BonusValue;

	private bool m_isMoving;
	private Vector2 m_dir;

	public void StartMoving(Vector2 dir, Vector2 pos) {
		
		D.Trace(LogCategory.Game, "[GameBonus] StartMoving");
		__startMoving(dir, pos);
	}
	
	private void __startMoving(Vector2 dir, Vector2 pos) {
		
		D.Trace(LogCategory.Game, "[GameBonus] __startMoving");

		m_dir = dir;
		transform.position = pos;
		m_isMoving = true;
		
		/*
		transform.position = (Vector3)ShapeLaneBegin;

		TweenParms tp = new TweenParms();
		tp.Prop("position", (Vector3)ShapeLaneEnd);
		tp.SpeedBased();
		tp.OnComplete(__tweenOnComplete);
		m_tweener = HOTween.To(transform, ShapeSpeed, tp);
		*/
	}
	void Update () {
		
		D.Fine(LogCategory.Game, "[GameBonus] Update");

		D.Watch("bonus ismoving", m_isMoving);

		if (!m_isMoving) return;

		D.Watch("bonus dir", m_dir);
		D.Watch("time", Time.time);

		if (transform.position.y < -1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		if (transform.position.y > 1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		if (transform.position.x < -1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}
		
		if (transform.position.x > 1000) {
			m_isMoving = false;
			Destroy(gameObject);
		}


		transform.Translate(m_dir * Time.deltaTime);
		
	}
}
