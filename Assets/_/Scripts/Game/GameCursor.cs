﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameCursor : MonoBehaviour {

	private Camera			m_mainCamera;
	private Ray2D			m_ray;
	private RaycastHit2D	m_raycastHit;

	private bool			m_dragging;
	private Shape			m_draggingShape;

	//	PUBLIC

	//	PRIVATE

	//	MONO


	void OnEnable() {

		D.Trace(LogCategory.SubSystem, "[GameCursor] OnEnable");
	}

	void Start () {
	
		D.Trace(LogCategory.SubSystem, "[GameCursor] Start");

		m_mainCamera = Camera.main;
	}
	
	void Update () {
	
		D.Fine(LogCategory.SubSystem, "[GameCursor] Update");

		m_raycastHit = GameController.Instance.GameInput.GetRaycastHit();
		if (m_raycastHit.transform != null) D.Watch("hit", m_raycastHit.transform.name);

		if (Input.GetMouseButtonDown(0)) {

			if (m_raycastHit) {

				if (m_raycastHit.transform.tag == "Shape") {
					D.Detail("- picking up shape");
					GameObject _go = m_raycastHit.transform.gameObject;
					m_draggingShape = (Shape)_go.GetComponent<Shape>();
					m_draggingShape.StartPickup();
					//	the dragging shape is actually the parent of the collider
					//m_draggingShape = (Shape)_go.transform.parent.GetComponent<Shape>();
					//m_draggingShape.Pause();
					m_draggingShape.transform.parent = transform;
					//HOTween.To(m_draggingShape.transform, 1, "localPosition", new Vector3(0,0,0)); 
					m_draggingShape.transform.localPosition = new Vector3(0,0,0);
					m_draggingShape.ShapeDragging = true;
					m_dragging = true;
				}

				if (m_raycastHit.transform.tag == "Bonus") {
					D.Detail("- picking up bonus");
					GameManager.Instance.GameEffects.PlayBonus(m_raycastHit.transform.position);
					GameManager.Instance.GameSounds.PlayBonusPickup();
					GameController.Instance.GameScore.AddScore(m_raycastHit.transform.GetComponent<GameBonus>().BonusValue);
					Destroy(m_raycastHit.transform.gameObject);
				}

				if (m_raycastHit.transform.tag == "Powerup") {
					D.Detail("- picking up powerup");
					string power = m_raycastHit.transform.GetComponent<GamePowerup>().PowerId;

					if (power == "PowerHealth") {
						GameManager.Instance.GameEffects.PlayPickupHeart(m_raycastHit.transform.position);
						GameController.Instance.GameLives.AddLives(1);
					}
					if (power == "PowerBlueShield") {
						GameController.Instance.Shifter.ActivateBlueShield();
					}
					if (power == "PowerGoldShield") {
						GameController.Instance.Shifter.ActivateConsumeMode();
					}

					GameManager.Instance.GameSounds.PlayPowerPickup();
					Destroy(m_raycastHit.transform.gameObject);
				}
			}
		}

		if (Input.GetMouseButtonUp(0)) {
			if (m_dragging) {
				D.Detail("- dropping shape");
				if (m_draggingShape != null) {
					m_draggingShape.transform.parent = null;
					m_draggingShape.ShapeDragging = false;
					m_draggingShape.transform.parent = GameObject.Find("_Shapes").transform;
					m_draggingShape.StopPickup();
				}
				m_dragging = false;
			}
		}

		if (m_dragging) {

			transform.position = GameController.Instance.GameInput.GetMousePosition();

		}

	}
}
