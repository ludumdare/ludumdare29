﻿
//	Copyright 2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class GameShapeManager : MonoBehaviour {

	public GameShape[]			GameShapes;

	private int					m_totalGameShapes;

	//	PUBLIC

	public GameShape GetRandomShape() {

		GameShape shape = (GameShape)Instantiate(GameShapes[Random.Range(0,m_totalGameShapes)]);
		if (shape == null) D.Warn("- could not generate a Random Shape");
		return shape;
	}

	public GameShape GetShape(string id) {

		GameShape _return = null;

		foreach(GameShape shape in GameShapes) {
			if (shape.ShapeId == id) {
				_return = (GameShape)Instantiate(shape);
			}
		}

		return _return;
	}

	//	PRIVATE

	//	MONO

	void OnEnable() {

		D.Trace("[GameShapeManager] OnEnable");
		m_totalGameShapes = GameShapes.Length;
		GameController.Instance.GameShapeManager = this;
	}

	void Start () {
	
		D.Trace("[GameShapeManager] Start");
	}
	
}
