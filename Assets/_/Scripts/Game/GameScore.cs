﻿using UnityEngine;
using System.Collections;

public class GameScore : MonoBehaviour {

	public int			Score		{ get; set; }
	public int			Multiplier	{ get; set; }

	private int			m_count;

	//	PUBLIC

	public void AddScore(int score) {
		Score += (score * Multiplier);
	}

	public void IncreaseMultiplier() {

		m_count += 1;

		if (m_count > 9) {
			Multiplier += 1;
			m_count =0;
			GameManager.Instance.GameSounds.PlayMultiplierUp();
		}

		if (Multiplier > 9) Multiplier = 9;

		D.Watch("count", m_count);
		D.Watch("multiplier", Multiplier);
	}

	public void ResetMultiplier() {

		Multiplier = 1;
		m_count = 0;

	}

	//	PRIVATE

	//	MONO

	void OnEnable() {

	}

	void Start () {

		GameController.Instance.GameScore = this;
		Multiplier = 1;
	}
	
	void Update () {
	
	}
}
