﻿
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameManager : MonoSingleton<GameManager> {

	//	PUBLIC
	
	public GameCamera			GameCamera			{ get; set; }
	public GameController		GameController		{ get; set; }
	public GameEffects			GameEffects			{ get; set; }
	public GameMusic			GameMusic			{ get; set; }
	public GameSettings			GameSettings		{ get; set; }
	public GameSounds			GameSounds			{ get; set; }
	public GameAnalytics		GameAnalytics		{ get; set; }

	public string				NextScene			{ get; set; }
	public string				LastScene			{ get; set; }
	
	public string 				CurrentLevel		{ get; set; }
	
	public void Init() {
		D.Trace("[GameManager] Init");
		__init();	
	}
	
	public void Play() {
	
		GameController.Play();
	}
	
	public void GotoNextScene() {
		
		D.Trace("[GameManager] GotoNextScene");
		Application.LoadLevel(NextScene);
	}

	public void GotoLastScene() {
		
		D.Trace("[GameManager] GotoLastScene");
		Application.LoadLevel(LastScene);
	}

	//	PRIVATE
	
	private void __init() {
		
		D.Trace("[GameManager] __init");
		GameAnalytics = new GameAnalytics();
		GameAnalytics.Init();
	}
	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[GameManager] OnEnable");
		GameSettings = new GameSettings();
		GameSettings.Load();
	}
	
	void Start() {
			
		D.Trace("[GameManager] Start");
	}
}
