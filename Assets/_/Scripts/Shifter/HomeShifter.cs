﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class HomeShifter : MonoBehaviour {

	private IEnumerator __moveShifter() {

		HOTween.To(transform, 2, "position", new Vector3(Random.Range(-400,400), Random.Range(-400,400), 0));
		yield return new WaitForSeconds(2);
		yield return StartCoroutine(__moveShifter());
	}

	// Use this for initialization
	void Start () {
		StartCoroutine(__moveShifter());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
