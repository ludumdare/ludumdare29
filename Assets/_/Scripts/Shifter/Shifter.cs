﻿
//	Copyright 2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Shifter : MonoBehaviour {

	private tk2dSprite		m_spriteBody;
	private tk2dSprite		m_spriteEyes;
	private tk2dSprite		m_spriteEyeBall;
	private tk2dSprite		m_spriteMouth;

	private GameShape		ShifterShape;
	private Color			ShifterColor;

	private GameObject		m_shieldObject;
	private GameObject		m_mouthObject;

	private bool			m_god;
	private bool			m_shield;
	private bool			m_consume;

	private Task			m_blueShieldTask;
	private Task 			m_consumeModeTask;
	private Task			m_shakeShifterTask;
	
	//	PUBLIC

	public void Pause() {
		if (m_blueShieldTask != null && m_blueShieldTask.Running) m_blueShieldTask.Pause();
		if (m_consumeModeTask != null && m_consumeModeTask.Running) m_consumeModeTask.Pause();
		if (m_shakeShifterTask != null && m_shakeShifterTask.Running) m_shakeShifterTask.Pause();
	}

	public void Unpause() {
		if (m_blueShieldTask != null && m_blueShieldTask.Paused) m_blueShieldTask.Unpause();
		if (m_consumeModeTask != null && m_consumeModeTask.Paused) m_consumeModeTask.Unpause();
		if (m_shakeShifterTask != null && m_shakeShifterTask.Paused) m_shakeShifterTask.Unpause();
	}

	//	generate a random shape
	public void RandomShift() {

		D.Trace(LogCategory.Game, "[Shifter] RandomShift");
	}

	//	try to consume a shape - returns false if failed
	public void ShapeShift(Shape shape) {

		D.Trace(LogCategory.Game, "[Shifter] ShapeShift");

		if (__canConsumeShape(shape) || m_consume) {
			__shapeShift(shape);
			GameManager.Instance.GameSounds.PlayConsumeShape();
			//	update scoring
			//GameController.Instance.GameTimer.AddTime(2);
			GameController.Instance.GameScore.AddScore(100);
			GameController.Instance.GameScore.IncreaseMultiplier();
			GameController.Instance.GameLevel.AddPoints(1);
		} else {
			__killShape(shape);
			GameManager.Instance.GameSounds.PlayBadShape();
			if (m_god) return;
			m_god = true;
			//StartCoroutine(__shakeCamera());
			m_shakeShifterTask = new Task(__shakeShifter());
			if (!GameController.Instance.GodMode) {
				//GameController.Instance.GameTimer.RemoveTime(5);
				GameController.Instance.GameScore.ResetMultiplier();
				GameController.Instance.GameLives.RemoveLives(1);
			}
		}
	}

	public void TriggerEnter(Collider2D other) {
		
		D.Trace(LogCategory.Game, "[Shifter] OnTriggerEnter");
		
		if (other.tag == "Shape") {
			D.Detail("- Shape has entered the Shifter", other.tag);
			Shape shape = other.transform.parent.GetComponent<Shape>();
			ShapeShift(shape);
		}
	}

	public void TriggerExit(Collider2D other) {
		
		D.Trace(LogCategory.Game, "[Shifter] OnTriggerExit");
		
		if (other.tag == "Shape") {
			D.Detail("- Shape has exited the Shifter", other.tag);
		}
	}

	public void ActivateBlueShield() {
		//StartCoroutine(__activateBlueShield());
		m_blueShieldTask = new Task(__activateBlueShield());

	}

	public void ActivateConsumeMode() {
		//StartCoroutine(__activateConsumer());
		m_consumeModeTask = new Task(__activateConsumer());
	}
	

	//	PRIVATE

	private IEnumerator __activateBlueShield() {

		m_god = true;
		m_shield = true;
		m_shieldObject.SetActive(true);
		yield return new WaitForSeconds(5);
		m_god = false;
		m_shield = false;
		m_shieldObject.SetActive(false);
		yield return null;
		
	}

	private IEnumerator __activateConsumer() {

		GameObject obj = GameManager.Instance.GameEffects.PlayAura(transform.position);
		obj.transform.parent = transform;
		Tweener t = HOTween.To(transform, 1, new TweenParms().Prop("position", new Vector3(0,0,0)).Loops(-1,LoopType.Yoyo));
		m_consume = true;
		tk2dSprite sprite = (tk2dSprite)transform.FindChild("#Shape").GetComponent<tk2dSprite>();
		Color color = sprite.color;
		int id = sprite.spriteId;
		GameController.Instance.GameSpawner.StopSpawner();
		int rate = GameController.Instance.GameSpawner.SpawnRate;
		int time = GameController.Instance.GameSpawner.SpawnTime;
		int speed = GameController.Instance.GameSpawner.SpawnSpeed;
		GameController.Instance.GameSpawner.SpawnRate = 240;
		GameController.Instance.GameSpawner.SpawnSpeed = 200;
		GameController.Instance.GameSpawner.StartSpawner();

		for(int i = 0; i < 120; i++) {
			sprite.spriteId = Random.Range(0, sprite.Collection.Count);
			sprite.color = GameController.Instance.GameSwatch.GetRandomShapeColor();
			yield return new WaitForSeconds(0.05f);
		}

		GameController.Instance.GameSpawner.StopSpawner();
		GameController.Instance.GameSpawner.SpawnRate = rate;
		GameController.Instance.GameSpawner.SpawnSpeed = speed;
		GameController.Instance.GameSpawner.StartSpawner();
		sprite.spriteId = id;
		sprite.color = color;
		m_consume = false;
		t.Kill();
		Destroy(obj);
		HOTween.To(transform, 1, new TweenParms().Prop("position", new Vector3(0,-150,0)));
	}

	private IEnumerator __shakeShifter() {

		Vector3 _cp = transform.position;
		Camera _c1 = GameObject.Find("GameCameraStars").GetComponent<Camera>();

		for(int i = 0; i < 40; i++) {
			yield return new WaitForSeconds(.005f);
			float x = transform.position.x + Random.Range(-5,5);
			float y = transform.position.y + Random.Range(-5,5);
			transform.position = new Vector2(x,y);
			_c1.backgroundColor = GameController.Instance.GameSwatch.GetRandomScreenShakeColor();
			yield return new WaitForSeconds(.005f);
			x = transform.position.x + Random.Range(-5,5);
			y = transform.position.y + Random.Range(-5,5);
			transform.position = new Vector2(x,y);
			_c1.backgroundColor = GameController.Instance.GameSwatch.GetRandomScreenShakeColor();
		}

		HOTween.To(transform, 1, "position", new Vector3(0,-150,0));
		_c1.backgroundColor = Color.black;

		m_god = false;
		yield return null;
	}

	private bool __shapeShift(Shape shape) {

		D.Trace(LogCategory.Game, "[Shifter] __shapeShift");

		D.Detail("- consuming Shape {0}", shape.ShapeId);

		if (!m_consume) {

			//	remove current shape
			if (transform.FindChild("#Shape")) Destroy(transform.FindChild("#Shape").gameObject);
			
			//	attach new shape
			GameShape _shape = (GameShape)GameController.Instance.GameShapeManager.GetShape(shape.ShapeId);
			__attachGameShape(_shape);
			_shape.GetComponent<tk2dSprite>().color = shape.ShapeColor;
			//	housekeeping
			ShifterColor = shape.ShapeColor;
			
			//	consume effect
			transform.localScale = new Vector3(1.2f, 1.2f, 1);
			HOTween.To(transform, 1, new TweenParms().Prop("localScale", new Vector3(1, 1, 1)).Ease(EaseType.EaseOutBounce));
		}

		//	destroy the shape
		Destroy(shape.gameObject);

		return true;
	}

	private void __attachGameShape(GameShape shape) {

		//	make it a child
		shape.gameObject.name = "#Shape";
		shape.gameObject.tag = "Shifter";
		shape.gameObject.SetActive(true);
		shape.transform.parent = this.transform;
		shape.transform.localPosition = new Vector3(0,0,0);
		
		//	rescale it
		shape.transform.localScale = new Vector3(0.50f, 0.50f, 1f);
		shape.GetComponent<tk2dSprite>().SortingOrder = -2;

		ShifterShape = shape;

	}

	private bool __canConsumeShape(Shape shape) {

		D.Trace(LogCategory.Game, "[Shifter] __canConsumeShape");

		bool _ret = false;

		if (shape.ShapeId == ShifterShape.ShapeId) _ret = true;
		if (shape.ShapeColor == ShifterColor) _ret = true;

		return _ret;
	}

	private void __killShape(Shape shape) {

		D.Trace(LogCategory.Game, "[Shifter] __killShape");

		//shape.collider.enabled = false;
		GameManager.Instance.GameEffects.PlaySmallHit(shape.transform.position);
		GameController.Instance.GameTimer.RemoveTime(5);
		GameController.Instance.GameScore.ResetMultiplier();
		shape.Kill();
	}

	//	MONO

	void OnEnable() {

		D.Trace(LogCategory.Game, "[Shifter] OnEnable");
		m_spriteBody = transform.GetComponent<tk2dSprite>();
		m_shieldObject = (GameObject)transform.FindChild("#Shield").gameObject;
		m_mouthObject = (GameObject)transform.FindChild("#Mouth").gameObject;
		m_shieldObject.SetActive(false);
		m_mouthObject.SetActive(false);
		//transform.GetComponent<Collider2D>().isTrigger = true;
	}

	void Start () {
	
		D.Trace(LogCategory.Game, "[Shifter] Start");
		GameController.Instance.Shifter = this;

		//	start with random shape
		GameShape shape = GameController.Instance.GameShapeManager.GetRandomShape();
		Color color = GameController.Instance.GameSwatch.GetRandomShapeColor();
		shape.GetComponent<tk2dSprite>().color = color;
		ShifterColor = color;
		__attachGameShape(shape);		
	}
	
}
