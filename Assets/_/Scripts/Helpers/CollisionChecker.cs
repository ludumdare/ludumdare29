﻿using UnityEngine;
using System.Collections;

public class CollisionChecker : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other) {

		//D.Warn(" - trigger enter with {0}", other.tag);
		SendMessageUpwards("TriggerEnter", other, SendMessageOptions.RequireReceiver);
	}
	void OnTriggerExit2D(Collider2D other) {
		
		//D.Warn(" - trigger exit with {0}", other.tag);
		SendMessageUpwards("TriggerExit", other, SendMessageOptions.RequireReceiver);
	}
}
