﻿using UnityEngine;
using System.Collections;

public class RayCaster : MonoBehaviour {

	public bool					m_rayShow;
	public float				m_raySize;
	public Vector3				m_rayOffset;
	public int					m_rayIgnoreLayer;
	public int					m_rayInterval = 45;	//	default is 45!!
	
	public RaycastHit			Hit		{ get; set; }
	
	//	PUBLIC
	
	//	PRIVATE
	
	private RaycastHit __raycast() {
		
		D.Fine("[RayCaster] __raycast");
		
		RaycastHit _hit = new RaycastHit();
		RaycastHit _return = new RaycastHit();
		int _ignoreLayer = ~(1 << m_rayIgnoreLayer);
		
		for (int i = 0; i < 360; i += m_rayInterval) {
			
			if (m_rayShow) Debug.DrawRay(m_rayOffset + transform.position,(Quaternion.Euler(90,90,i) * Vector3.up * m_raySize), Color.grey);
			
			if (Physics.Raycast(m_rayOffset + transform.position, Quaternion.Euler(90,90,i) * Vector3.up, out _hit, m_raySize, _ignoreLayer)) {
				
				if (_hit.transform.tag == "Player") {
					if (m_rayShow) Debug.DrawLine(transform.position, _hit.point, Color.blue);
				}
				
				if (_hit.transform.tag == "Enemy") {
					if (m_rayShow) Debug.DrawLine(transform.position, _hit.point, Color.red);
				}
				
				if (_hit.transform.tag == "Object") {
					if (m_rayShow) Debug.DrawLine(transform.position, _hit.point, Color.white);
				}
				
				_return = _hit;
				break;
			}
		}
		
		return _return;
	}
	
	//	MONO
	
	void Update () {
		
		D.Fine("[EnemyController] Update");
		Hit = __raycast();		
	}
}
