// Copyright 2012, SpockerDotNet LLC

/*
	Thanks to codebender for his article here 
	http://codebender.denniland.com/a-singleton-base-class-to-implement-the-singleton-pattern-in-c/
	on designing a Singleton base class
	
	How to use this class:
	
	// Must seal the class so no sub-classes can be defined. 
	
	public sealed class SequenceGeneratorSingleton
		: SingletonBase<SequenceGeneratorSingleton>
	{
    	// Must have a private constructor so no instance can be created externally.
    	SequenceGeneratorSingleton()
    	{
        _	number = 0;
    	}
 
    	private int _number;
 
    	public int GetSequenceNumber()
    	{
        	return _number++;
    	}
	}
	
	// and then to use the class
	
	 Console.WriteLine("Sequence: " + SequenceGeneratorSingleton.Instance
            .GetSequenceNumber().ToString());  // Print "Sequence: 0"
	
*/


using System;
using System.Globalization;
using System.Reflection;

public abstract class Singleton<T> where T : class {

    /// <summary>
    /// A protected constructor which is accessible only to the sub classes.
    /// </summary>
    protected Singleton() { }
 
    /// <summary>
    /// Gets the singleton instance of this class.
    /// </summary>
    public static T Instance
    {
        get { return SingletonFactory.Instance; }
    }
 
    /// <summary>
    /// The singleton class factory to create the singleton instance.
    /// </summary>
    class SingletonFactory
    {
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static SingletonFactory() { }
 
        // Prevent the compiler from generating a default constructor.
        SingletonFactory() { }
 
        internal static readonly T Instance = GetInstance();
 
        static T GetInstance()
        {
            var theType = typeof(T);
 
            T inst;
 
            try
            {
                inst = (T)theType
                    .InvokeMember(theType.Name,
                    BindingFlags.CreateInstance | BindingFlags.Instance
                    | BindingFlags.NonPublic,
                    null, null, null,
                    CultureInfo.InvariantCulture);
            }
            catch (MissingMethodException ex)
            {
                throw new TypeLoadException(string.Format(
                    CultureInfo.CurrentCulture,
                    "The type '{0}' must have a private constructor to " +
                    "be used in the Singleton pattern.", theType.FullName)
                    , ex);
            }
 
            return inst;
        }
    }
}
