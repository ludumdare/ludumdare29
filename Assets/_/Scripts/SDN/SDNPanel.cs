﻿
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class SDNPanel : MonoBehaviour {

	public bool			m_autoHide;
	public Vector2		m_showPosition;
	public Vector2		m_hidePosition;
	public float		m_easeDuration;
	public EaseType		m_easeType;
	
	public virtual void OnShow() { }
	public virtual void OnStart() { }
	
	public virtual void Hide() {
		D.Trace("[SDNPanel] Hide");
		__hide();
	}
	
	public void Show() {
		D.Trace("[SDNPanel] Show");
		__show();
	}
	
	void OnEnable() {
		D.Trace("[SDNPanel] OnEnable");
	}

	void Start() {

		D.Trace("[SDNPanel] Start");
		if (m_autoHide) transform.localPosition = m_hidePosition;
		OnStart();
	}
	
	private Vector3 __newLocalPos(Vector2 vector) {
		D.Trace("[SDNPanel] __newLocalPos");
		D.Detail("- new localPosition is ({0}, {1})", vector.x, vector.y);
		return new Vector3(vector.x, vector.y, transform.localPosition.z);
	}
	

	private void __show() {
		
		D.Trace("[SDNPanel] __show");
		//transform.localPosition = __newLocalPos(m_showPosition);
		//HOTween.To(transform, 0.5f, "localPosition", __newLocalPos(m_showPosition));
		__moveTo(__newLocalPos(m_showPosition));
		OnShow();
	}
	
	private void __hide() {
		D.Trace("[SDNPanel] __hide");
		//transform.localPosition = __newLocalPos(m_hidePosition);
		//HOTween.To(transform, 0.5f, "localPosition", __newLocalPos(m_hidePosition));
		__moveTo(__newLocalPos(m_hidePosition));
	}
	
	private void __moveTo(Vector3 pos) {
		D.Trace("[SDNPanel] __moveTo");
		TweenParms tp = new TweenParms();
		tp.Prop("localPosition", pos);
		tp.Ease(m_easeType);
		D.Detail("- moving to localPosition ({0}, {1})", pos.x, pos.y);
		HOTween.To(transform, m_easeDuration, tp);
	}
	
	
}
