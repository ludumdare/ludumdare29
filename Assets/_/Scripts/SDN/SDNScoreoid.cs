﻿

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SDNScoreoid : MonoSingleton<SDNScoreoid> {
	
	private string			m_usageKey;

	//	===	PUBLIC
	
	public void CreateScore(string player, int score) {
		
		D.Trace("[SDNScoreoid] CreateScore");
		D.Detail("- creating http package to create score");
		
		WWWForm form = new WWWForm();
		form.AddField("api_key", GameManager.Instance.GameSettings.ScoreoidApi);
		form.AddField("game_id", GameManager.Instance.GameSettings.ScoreoidGame);
		form.AddField("username", player);
		form.AddField("score", score);
		form.AddField("platform", "ld29");
		WWW www = new WWW( GameManager.Instance.GameSettings.ScoreoidUrl + "/createScore", form);
		
		HTTP.Instance.HTTP_Request(www, __sdnResponse);	
	}
	
	public void CreateScoreLevel(string player, int level) {
		
		D.Trace("[SDNScoreoid] CreateScore");
		D.Detail("- creating http package to create score");
		
		WWWForm form = new WWWForm();
		form.AddField("api_key", GameManager.Instance.GameSettings.ScoreoidApi);
		form.AddField("game_id", GameManager.Instance.GameSettings.ScoreoidGame);
		form.AddField("username", player);
		form.AddField("score", level);
		form.AddField("platform", "ld29level");
		WWW www = new WWW( GameManager.Instance.GameSettings.ScoreoidUrl + "/createScore", form);
		
		HTTP.Instance.HTTP_Request(www, __sdnResponse);	
	}
	
	public void GetBestScores() {
		
		D.Trace("[SDNScoreoid] GetBestScore");
		D.Detail("- creating http package to get best scores");
		
		WWWForm form = new WWWForm();
		form.AddField("api_key", GameManager.Instance.GameSettings.ScoreoidApi);
		form.AddField("game_id", GameManager.Instance.GameSettings.ScoreoidGame);
		form.AddField("platform", "ld29");
		form.AddField("limit", "25");
		form.AddField("order_by", "score");
		form.AddField("order", "desc");
		WWW www = new WWW( GameManager.Instance.GameSettings.ScoreoidUrl + "/getBestScores", form);
		
		HTTP.Instance.HTTP_Request(www, __getBestScores);	
	}
	
	public void GetBestLevels() {
		
		D.Trace("[SDNScoreoid] GetBestLevels");
		D.Detail("- creating http package to get best scores");
		
		WWWForm form = new WWWForm();
		form.AddField("api_key", GameManager.Instance.GameSettings.ScoreoidApi);
		form.AddField("game_id", GameManager.Instance.GameSettings.ScoreoidGame);
		form.AddField("platform", "ld29level");
		form.AddField("limit", "25");
		form.AddField("order_by", "score");
		form.AddField("order", "desc");
		WWW www = new WWW( GameManager.Instance.GameSettings.ScoreoidUrl + "/getBestScores", form);
		
		HTTP.Instance.HTTP_Request(www, __getBestLevels);	
	}
	

	//	===	PRIVATE
	

	private void __getBestScores(WWW www) {
		
		D.Trace("[SDNScoreoid] __getBestScores");
		ScoreoidEvent r = __returnData(www);
		ScoreoidGetBestScoresEvent e = new ScoreoidGetBestScoresEvent();
		List<object> l = MiniJSON.Json.Deserialize((string)r.Data) as List<object>;
		e.Error = r.Error;
		e.Data = l;
		GameEvents.Instance.Raise(e);
	}
	
	private void __getBestLevels(WWW www) {
		
		D.Trace("[SDNScoreoid] __getBestLevels");
		ScoreoidEvent r = __returnData(www);
		ScoreoidGetBestLevelsEvent e = new ScoreoidGetBestLevelsEvent();
		List<object> l = MiniJSON.Json.Deserialize((string)r.Data) as List<object>;
		e.Error = r.Error;
		e.Data = l;
		GameEvents.Instance.Raise(e);
	}
	
	private ScoreoidEvent __returnData(WWW www) {
		
		D.Trace("[SDNScoreoid] __returnData");
		D.Detail(www.text);
		ScoreoidEvent e = new ScoreoidEvent();
		e.Error = false;
		e.Data = "";

		if (string.IsNullOrEmpty(www.error)) {
			e.Data = www.text;
		} else {
			e.Error = true;	
		}
		
		return e;
		
	}
	
	private void __sdnResponse(WWW www) {

		D.Log(www.text);

		if (!string.IsNullOrEmpty(www.error)) {
			D.Warn("An Error Occured Submitting to Scoreoid");
		}
		
	}
	
	//	===	MONO
	
	void OnEnable() {
	
	}
	
	void OnApplicationQuit() {
		
		D.Trace("[SDNScoreoid] OnApplicationQuit");
		//Application.CancelQuit();
		//SavePlayerDataQuit();
	}
	
	
	
}
