﻿using UnityEngine;
using System.Collections;

public class SDNDelayDestroy : MonoBehaviour {
	
	public float			m_delay;
	
	// Use this for initialization
	void Start () {
		StartCoroutine(__destroy());
	}
	
	private IEnumerator __destroy() {
		
		D.Trace("[SDNDelayDestroy] __destroy");
		yield return new WaitForSeconds(m_delay);
		DestroyImmediate(gameObject);
	}
	
}
