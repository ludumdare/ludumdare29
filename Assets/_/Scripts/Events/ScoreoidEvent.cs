﻿using UnityEngine;
using System.Collections;

public class ScoreoidEvent : GameEvent {

	public bool					Error { get; set; }
	public object				Data { get; set; }
}
