﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelPanel : SDNPanel {

	public GameObject		m_item;
	
	private void __getBestLevels(ScoreoidGetBestLevelsEvent e) {
		
		List<object> l = e.Data as List<object>;
		D.Detail("- scoreoid returned {0} scores", l.Count);
		
		GameObject container = (GameObject)transform.FindChild("@Container").gameObject;
		
		int row = 0;
		
		foreach(object ls in l) {
			
			GameObject go = (GameObject)Instantiate(m_item);
			go.transform.parent = container.transform;
			go.SetActive(true);
			go.transform.localPosition = new Vector3(0, row * -70,0);
			Dictionary<string, object> d = ls as Dictionary<string, object>;
			string p = "";
			int s = 0;
			if (d.ContainsKey("Player")) {
				Dictionary<string, object> dp = d["Player"] as Dictionary<string, object>;
				p = (string) dp["username"]; // aka alias
			}
			if (d.ContainsKey("Score")) {
				Dictionary<string, object> ds = d["Score"] as Dictionary<string, object>;
				s = (int.Parse((string)ds["score"]));
			}
			
			go.transform.FindChild("@Name").GetComponent<tk2dTextMesh>().text = p;
			go.transform.FindChild("@Level").GetComponent<tk2dTextMesh>().text = s.ToString();
			
			row += 1;
		}
		
	}
	
	void OnEnable() {
		GameEvents.Instance.AddListener<ScoreoidGetBestLevelsEvent>(__getBestLevels);
	}
	
	void OnDisable() {
		GameEvents.Instance.RemoveListener<ScoreoidGetBestLevelsEvent>(__getBestLevels);
	}

	public override void OnStart () {
		SDNScoreoid.Instance.GetBestLevels();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
