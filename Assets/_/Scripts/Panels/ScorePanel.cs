﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScorePanel : SDNPanel {

	public GameObject		m_item;

	private void __getBestScores(ScoreoidGetBestScoresEvent e) {

		List<object> l = e.Data as List<object>;
		D.Detail("- scoreoid returned {0} scores", l.Count);

		GameObject container = (GameObject)transform.FindChild("@Container").gameObject;

		int row = 0;

		foreach(object ls in l) {

			GameObject go = (GameObject)Instantiate(m_item);
			go.transform.parent = container.transform;
			go.SetActive(true);
			go.transform.localPosition = new Vector3(0, row * -65,0);
			Dictionary<string, object> d = ls as Dictionary<string, object>;
			string p = "";
			int s = 0;
			if (d.ContainsKey("Player")) {
				Dictionary<string, object> dp = d["Player"] as Dictionary<string, object>;
				p = (string) dp["username"]; // aka alias
			}
			if (d.ContainsKey("Score")) {
				Dictionary<string, object> ds = d["Score"] as Dictionary<string, object>;
				s = (int.Parse((string)ds["score"]));
			}

			go.transform.FindChild("@Name").GetComponent<tk2dTextMesh>().text = p;
			go.transform.FindChild("@Score").GetComponent<tk2dTextMesh>().text = s.ToString();

			row += 1;
		}
		Show();
	}


	void OnEnable() {

		GameEvents.Instance.AddListener<ScoreoidGetBestScoresEvent>(__getBestScores);
	}

	void OnDisable() {

		GameEvents.Instance.RemoveListener<ScoreoidGetBestScoresEvent>(__getBestScores);
	}

	public override void OnStart () {
		SDNScoreoid.Instance.GetBestScores();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
