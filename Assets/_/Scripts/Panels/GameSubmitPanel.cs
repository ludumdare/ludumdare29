﻿using UnityEngine;
using System.Collections;

public class GameSubmitPanel : SDNPanel {

	public tk2dUITextInput		m_textName;

	// Use this for initialization
	public void SubmitButton_Click() {
		Hide();
		GameManager.Instance.GameSettings.Username = m_textName.Text;
		SDNScoreoid.Instance.CreateScore(m_textName.Text, GameController.Instance.GameScore.Score);
		SDNScoreoid.Instance.CreateScoreLevel(m_textName.Text, GameController.Instance.GameLevel.Level);
		GameController.Instance.GameOver();
	}
	
}
