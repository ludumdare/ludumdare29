﻿using UnityEngine;
using System.Collections;

public class WorkspaceShapePanel : MonoBehaviour {

	private Shape			m_shape;

	//	PUBLIC

	public void GenerateButton_Click() {

		m_shape.GenerateShape();

	}

	//	PRIVATE

	//	MONO

	void OnEnable() {

		m_shape = GameObject.Find("Shape").GetComponent<Shape>();

		if (m_shape == null) {
			D.Warn("- could not find Shape object in Scene");
		}
	}

	void Start () {
	
	}
	
	void Update () {
	
	}
}
