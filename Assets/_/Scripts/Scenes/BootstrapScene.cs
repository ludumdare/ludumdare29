﻿using UnityEngine;
using System.Collections;

public class BootstrapScene : MonoBehaviour {

	void OnEnable() {
		
		D.Log("[BootstrapScene] OnEnable");

		BootstrapStart.Start();
		BootstrapGame.Start();
		BootstrapPlayer.Start();
		BootstrapCloud.Start();
		BootstrapGraphics.Start();
		BootstrapEnd.Start();
		
	}
		
}
