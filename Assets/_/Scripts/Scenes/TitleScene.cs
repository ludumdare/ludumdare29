﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class TitleScene : MonoBehaviour {

	public tk2dSprite		m_titleLogo;
	public Camera			m_titleCamera;


	private IEnumerator	__open() {

		m_titleLogo.color = new Color(1,1,1,0);
		m_titleCamera.backgroundColor = Color.black;
		HOTween.To(m_titleCamera, 2, "backgroundColor", Color.white);
		yield return new WaitForSeconds(2);
		HOTween.To(m_titleLogo, 2, "color", new Color(1,1,1,1));
		yield return new WaitForSeconds(2);
		HOTween.To(m_titleLogo, 2, "color", new Color(1,1,1,0));
		yield return new WaitForSeconds(2);
		HOTween.To(m_titleCamera, 2, "backgroundColor", Color.black);
		yield return new WaitForSeconds(2);
		Application.LoadLevel("@Home");
		yield break;
	}

	// Use this for initialization
	void Start () {

		Task m = new Task(__open());
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
