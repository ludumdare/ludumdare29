﻿using UnityEngine;
using System.Collections;

public class WorkspaceScene : MonoBehaviour {

	public string			GotoScene;
	public bool				GotoSceneActive;

	private string			m_scene;
	private bool			m_sceneActivated;

	//	PUBLIC

	//	PRIVATE

	//	MONO

	void OnEnable() {

		D.Trace(LogCategory.System, "[WorkspaceScene] OnEnable");
		m_scene = GotoScene;
		m_sceneActivated = GotoSceneActive;
	}

	void Start () {

		D.Trace(LogCategory.System, "[WorkspaceScene] Start");
		if (m_sceneActivated) Application.LoadLevel(m_scene);
	
	}
	
	void Update () {
	
		D.Fine(LogCategory.System, "[WorkspaceScene] Fine");
	}
}
