﻿using UnityEngine;
using System.Collections;

public class HomeScene : MonoBehaviour {

	public void PlayButton__Click() {
		Application.LoadLevel("@Game");
	}

	public void AboutButton_Click() {
		Application.LoadLevel("@About");
	}

	public void ScoresButton_Click() {
		Application.LoadLevel("@Scores");
	}

	public void OptionButton_Click() {
		Application.LoadLevel("@Options");
	}

	void OnEnable() {
	}

	void Start() {
		GameManager.Instance.GameMusic.PlayIntroMusic();
	}
}
