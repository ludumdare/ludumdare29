﻿using UnityEngine;
using System.Collections;
using EleckTek;

public class GameScene : MonoBehaviour {

	private bool		m_pause = false;

	//	PUBLIC

	public void PauseButton_Click() {
		m_pause = true;
		GameObject.Find("tk2dUIManager").tag = "HOTween";
		Holoville.HOTween.HOTween.Pause();
		GameController.Instance.Shifter.Pause();
		GameManager.Instance.GameMusic.Pause();
		GameController.Instance.GameSpawner.StopSpawner();
		GameObject.Find("@Pause").GetComponent<PauseController>().ActivatePauseProtocol();
		GameObject.Find("GamePausePanel").GetComponent<SDNPanel>().Show();
		GameObject.Find("PauseButton").GetComponent<SDNPanel>().Hide();
	}

	public void UnpauseButton_Click() {
		m_pause = false;
		GameObject.Find("@Pause").GetComponent<PauseController>().DeactivatePauseProtocol();
		GameController.Instance.Shifter.Unpause();
		GameManager.Instance.GameMusic.Play();
		Holoville.HOTween.HOTween.Play();
		GameController.Instance.GameSpawner.StartSpawner();
		GameObject.Find("GamePausePanel").GetComponent<SDNPanel>().Hide();
		GameObject.Find("PauseButton").GetComponent<SDNPanel>().Show();
	}

	public void ReturnButton_Click() {
		GameObject.Find("@Pause").GetComponent<PauseController>().DeactivatePauseProtocol();
		Application.LoadLevel("@Home");
	}

	//	PRIVATE

	//	MONO

	void OnEnable() {
	}

	void Start () {
		GameManager.Instance.Play();
	}

	void Update() {

		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (!m_pause) {
				PauseButton_Click();
			} else {
				UnpauseButton_Click();
			}
		}
	}
	
}
