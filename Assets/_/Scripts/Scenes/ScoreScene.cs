﻿using UnityEngine;
using System.Collections;

public class ScoreScene : MonoBehaviour {

	public void ScoreButton_Click() {

		__hidePanels();
		GameObject.Find("ScorePanel").GetComponent<SDNPanel>().Show();
	}

	public void LevelButton_Click() {

		__hidePanels();
		GameObject.Find("LevelPanel").GetComponent<SDNPanel>().Show();
	}

	public void StatsButton_Click() {

		__hidePanels();
	}

	public void Home_Click() {

		Application.LoadLevel("@Home");
	}

	void Start() {
		GameManager.Instance.GameMusic.PlayIntroMusic();
	}

	private void __hidePanels() {

		GameObject.Find("ScorePanel").GetComponent<SDNPanel>().Hide();
		GameObject.Find("LevelPanel").GetComponent<SDNPanel>().Hide();
	}
}
