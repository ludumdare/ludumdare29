﻿
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using Simple;

public static class BootstrapStart {

	public static void Start() {
		
		Debug.LogWarning("[BootstrapStart] Start");	

		Holoville.HOTween.HOTween.Init(true, false, true);
		GameObject.Find("HOTween").tag = "HOTween";

		GameManager.Instance.Init();
		
		//	init logger
		
		D.Log(GameManager.Instance.GameSettings.GameName);
		D.Log(GameManager.Instance.GameSettings.GameCopyright);
		
		//	init simple console commands
		
		D.Log(LogCategory.System, "- creating simple console commands");
		
		SimpleConsole.AddCommand("hello", "Shows Hello World", __hello);
		SimpleConsole.AddCommand("sw", "Show Watches", __showWatches);
		SimpleConsole.AddCommand("hw", "Hide Watches", __hideWatches);
		SimpleConsole.AddCommand("play", "Play Level", __playLevel);
		SimpleConsole.AddCommand("addp", "Add Points", __addPoints);
		SimpleConsole.AddCommand("god", "Toggle God Mode", __godMode);
		SimpleConsole.AddCommand("lvl", "Jump To Level", __changeLevel);
		SimpleConsole.AddCommand("blue", "Activate Blue Shield", __blueShield);
		SimpleConsole.AddCommand("cons", "Activate Consumer", __consumeMode);
		SimpleConsole.AddCommand("life", "Add Extra Life", __addLife);

		//	analytics - start game (unless we are in the editor)
		if (!Application.isEditor) {
			GameManager.Instance.GameAnalytics.Active = true;
			GameManager.Instance.GameAnalytics.Log("game", "start", 1);
		}
	}
	


	private static string __hello(SimpleContainer args) {
		return "Hello World";	
	}

	private static string __showWatches(SimpleContainer args) {
		DLogger.Instance.m_showWatches = true;
		return "";
	}

	private static string __hideWatches(SimpleContainer args) {
		DLogger.Instance.m_showWatches = false;
		return "";
	}
	
	private static string  __playLevel(SimpleContainer args) {
		string level = args.getAtIndex(0).sData;
		GameManager.Instance.CurrentLevel = level;
		Application.LoadLevel("Game");
		return "Starting level " + level;
	}
	
	private static string  __addPoints(SimpleContainer args) {
		int points = args.getAtIndex(0).iData;
		GameManager.Instance.GameController.GameLevel.AddPoints(points);
		return "Add Points " + points;
	}

	private static string __godMode(SimpleContainer args) {
		GameController.Instance.GodMode = !GameController.Instance.GodMode;
		return "God Mode is " + GameController.Instance.GodMode;
	}
	
	private static string  __changeLevel(SimpleContainer args) {
		int level = args.getAtIndex(0).iData;
		GameManager.Instance.GameController.GameLevel.ChangeLevel(level);
		return "Jumping to Level " + level;
	}
	
	private static string __blueShield(SimpleContainer args) {
		GameController.Instance.Shifter.ActivateBlueShield();
		return "Blue Shield is Activated";
	}
	
	private static string __addLife(SimpleContainer args) {
		GameController.Instance.GameLives.AddLives(1);
		return "Added Life";
	}
	
	private static string __consumeMode(SimpleContainer args) {
		GameController.Instance.Shifter.ActivateConsumeMode();
		return "Connsume Mode Activated";
	}
}
